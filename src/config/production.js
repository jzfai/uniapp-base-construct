//production.js
module.exports = {
  env: 'production', //环境名称
  VUE_APP_BASE_URL: 'https://vhs.ruixiude.com/vhs/v1',//请求的url地址
  VUE_APP_BASE_IMAGE_URL: 'https://vhs.ruixiude.com'
}
