//index.js
var development = require('./development')
var production = require('./production')
// var test = require('../config/test')
// var production = require('../config/production')
module.exports = {
  // test: test,
  development,
  production,
}[process.env.NODE_ENV || 'development']
