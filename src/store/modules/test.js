const state = {
   test:1
}

const mutations = {
  M_test: (state,data) => {
	  console.log("mutationsM_test",data);
  },
}

const actions = {
  // user login
  A_test({ commit,state }, data) {
	  console.log( "actionsA_test");
	  commit('M_test', data)
  },

}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
