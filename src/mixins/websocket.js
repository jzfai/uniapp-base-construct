
export default {
  data() {
    return {
      /* webSocket推送的数据*/
      /* 重连*/
      taskSocket: null,
      webSocketTaskData: {},
      showVehivlesTaskId: null,
      socketReconnectNum: 0, // 不能超过5次
      lockReconnect: false,
      // 心跳机制
      webSocket: null,
      setHearTime: 5000,
      heartTimeOut: 15000,
      timeoutObj: null,
      serverTimeoutObj: null
    }
  },
  methods: {
    /* 心跳*/
    heartReset() {
      this.timeoutObj && clearTimeout(this.timeoutObj)
      this.serverTimeoutObj && clearTimeout(this.serverTimeoutObj)
    },
    heartStart() {
      this.timeoutObj && clearTimeout(this.timeoutObj)
      this.serverTimeoutObj && clearTimeout(this.serverTimeoutObj)
      this.timeoutObj = setTimeout(() => {
        this.socket.send('HeartBeat')
        this.serverTimeoutObj = setTimeout(() => {
          this.socketReconnect()
        }, this.heartTimeOut)
      }, this.setHearTime)
    },
    //最多可以连接7次
    async socketReconnect() {
      if (this.socketReconnectNum > 7 || this.lockReconnect) return
      this.socketReconnectNum++
      await this.$comentUtil.sleep(6000)
      this.lockReconnect = true
      // this.socketConnect()
    },
    resetSocketReconnect() {
      this.socketReconnectNum = 0
      this.lockReconnect = false
    }
  }
}
