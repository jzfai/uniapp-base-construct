import Vue from 'vue'
import Vuex from 'vuex'
import App from './App'
import store from './store'

// https
import uniRequest from '@/utils/uniRequest'
Vue.prototype.$uniRequest = uniRequest

//mixin全局混入
import globalMixin from '@/mixins/globalMixin.js'
Vue.mixin(globalMixin)

//注册全局组件
// 引入过滤器
import dataFilters from '@/filters/dataFilters'
Object.keys(dataFilters).forEach(key => {
  Vue.filter(key, dataFilters[key])
})

// #ifndef APP-NVUE
/*引入uview*/
import uView from "uview-ui";
Vue.use(uView);
// #endif


// moment-mini
import momentMini from 'moment-mini'
Vue.prototype.$momentMini = momentMini


// comentUtil
import comentUtil from '@/utils/comentUtil'
Vue.prototype.$comentUtil = comentUtil

/* 引入全局组件 */
import BottomPullR from '@/globalComponent/BottomPullR.vue'
import NDBottomPullR from '@/globalComponent/NDBottomPullR.vue'
import PageNavigationBar from '@/globalComponent/PageNavigationBar.vue'
import CenterShowModal from '@/globalComponent/CenterShowModal.vue'
import BtnUpModal from '@/globalComponent/BtnUpModal.vue'
import VueTabBar from '@/globalComponent/VueTabBar.vue'
import MosoweCanvasImage from '@/components/mosowe-canvas-image/mosowe-canvas-image.vue';
Vue.component('BottomPullR', BottomPullR)
Vue.component('NDBottomPullR', NDBottomPullR)
Vue.component('MosoweCanvasImage', MosoweCanvasImage)
Vue.component('PageNavigationBar', PageNavigationBar)
Vue.component('VueTabBar', VueTabBar)
Vue.component('CenterShowModal', CenterShowModal)
Vue.component('BtnUpModal', BtnUpModal)

Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
	store,
	...App
	
})
app.$mount()
