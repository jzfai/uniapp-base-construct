import momentMini from 'moment-mini'

export default {
  timeFormat(data, format) {
    return momentMini(data).format(format||'YYYY-MM-DD HH:mm:ss')
  }
}
