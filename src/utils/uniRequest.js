import uni_request from './uni_request.js'
const Qs = require('qs')
let isHALoading=true;
let config = require('../config');
const request = uni_request({
  baseURL:config.VUE_APP_BASE_URL, //baseURL
  timeout: 8000, // 超时时间
  header: {'x-custom-header': 'x-custom-header'}, // 设置请求头，建议放在请求拦截器中
  statusCode: [200, 401, 1000,0] // 服务器相应状态码为 200/401 时，网络请求不会 reject
})

request.interceptors.request.use(async (config, ...args) => {
  //console.log("req", args);
  return config
})
request.interceptors.response.use(async (response, ...args) => { // 响应拦截器（可以设置多个, 同时可以也可以使用异步方法）
  //console.log("response", response);
  //关闭微信loading
  uni.hideLoading()
  if (response.data&&(response.data.code === 200||response.data.reasonCode === 200||response.statusCode=== 200)) {
    return response.data
  }else{
    if(response.data&&response.data.message){
      uni.showToast({
        title: `${response.data&&response.data.message}`,
        icon: 'none',
        duration: 3000
      })
    }
  }
})
//全局错误监听
request.onerror = async (...args) => { // 请求失败统一处理方法（可以也可以使用异步方法）
  //关闭loading
  uni.showToast({
    title: `服务器或网络异常`,
    icon: 'none',
    duration: 3000
  })
  uni.hideLoading()
  console.log('网络请求失败了', `url为${args[1]}`)
}
export default function uniRequest({ url, data, method, isParams, isSBLoading,isHALoading}) {
  //参数发送的形式
  if (isParams) url=`${url}?`+Qs.stringify(data)
  //是否Loading
  if (isSBLoading) uni.showLoading({title: '数据加载中..', mask: true})
  isHALoading=isHALoading||true;
  return request[method](url,data)
}
