基础架构地址： https://gitee.com/jzfai/uniapp-base-construct.git
此架构主要用户开发移动端h5(包括微信小程序，嵌入h5,原生android和ios等)
在此架构处全局可以使用px单位书写，因为在postcss.config.js做了px转upx的配置
#### 前言

```
本架构采用：
基础架构：uniapp + vue2.6 + webpack 4.0
请求api: "uniapp自带"
时间格式化: "moment-mini": "^2.22.1",
地图： 高德地图
多环境配置 等
封装的组件：底部弹框，中部弹框,自定义顶部导航组件，自定义底部选项卡组件等
封装的api: 请求的api(类似axios),路由跳转(json路由传参)等
封装的css样式：flex弹性布局，居中布局，底部固定布局等
```
> uniapp开发需要部分微信小程序的基础，如果未开发过微信小程序请先学习微信小程序开发
#### 使用：
git clone https://gitee.com/jzfai/uniapp-base-construct.git
cd uniapp-base-construct
npm install 
使用cnpm 安装依赖时会有些包安装不到所以建议先使用npm安装，在使用cnpm安装一遍
npm run  dev:mp-weixin
#### 前提注意：

```
在此架构处全局可以使用px单位书写，因为在postcss.config.js做了px转upx的配置,引入了postcss-pxtorpx-pro进行转换
```

![1615280913989.png](https://upload-images.jianshu.io/upload_images/21080604-ee12b1c378104243.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


#### 规范建议

```
1.全部采用scss语言书写 且嵌套一般不超过3层
2.js方法和css的class，命名尽量唯一，不建议使用（map,socket，name,title）后期不以利维护
```

### 架构详解

#### 1页面配置

#### 1.1页面路径配置

![1615276309429.png](https://upload-images.jianshu.io/upload_images/21080604-af2bcbf1fbc5d53d.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)



##### 1.2页面底部table配置（此处配置了后，底部会显示底部标签选项，至少两个）

![1615276749697.png](https://upload-images.jianshu.io/upload_images/21080604-fac73390dfbff891.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


#### 2.VUEX（和pc端架构基本一致）

```
VUEX直接增加一个.js文件就能使用 如： experimentCenter.js
应为在index.js文件中已经配置了自定引入
```

![1609919889125.png](https://upload-images.jianshu.io/upload_images/21080604-a720068d59354d14.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


###### 2.1如何使用

```
1.引入
import { mapState, mapMutations, mapActions } from 'vuex'
1.1 state
computed: {
  ...mapState({
     webSocketPullData: state => state.vehicleMonitor.webSocketPullData
  })
},
console.log(this.webSocketPullData)

1.2 或者用commit方法（推荐）
 this.$store.commit('tagsView/M_DEL_CACHED_VIEW',"TestIndex")
 this.$store.commit('tagsView/M_ADD_CACHED_VIEW',"TestIndex")
```

>所有mutation 建议以 M_开头 ； 如：M_ADD_CACHED_VIEW
>所有 Actions建议以 A_开头 ； 如：A_ADD_CACHED_VIEW

#### 3.页面跳转

##### 3.1页面跳转

```
页面跳转已经在全局混合中配置
//跳转到不属于tab的页面（带参数跳转）
let data = {licenseNo: 11,name:"kuanghua" }
this.toNavigatePageMixin("/pages/navigateOne/navigateOne", data)
//跳转到属于tab的页面（带参数跳转）
let data = {licenseNo: 11,name:"kuanghua" }
this.toSwitchPageMixin("/pages/tableTwo/tableTwo", data)
```

#### 3.2 页面参数接收

```
this.paramsMixin 已在全局混合中配置
页面的参数接收可以通过this.paramsMixin读取
```

![1615278857239.png](https://upload-images.jianshu.io/upload_images/21080604-64c880f6eddd3232.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


#### 4.组件封装

#### 4.1全局组件位置

![1615279277968.png](https://upload-images.jianshu.io/upload_images/21080604-c2172e3a54581669.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)




>目前封装了的组件有：底部弹框，中部弹框，底部自定义tab，顶部自定义导航，canvas绘图（主要要用于生成海报，给图片加水印等）具体使用可以看tableOne页面的例子

#### 5.全局api的封装

![1615279750052.png](https://upload-images.jianshu.io/upload_images/21080604-9d3742d1158a5d69.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

```
gloalMixin.js是全局api文件，在main.js中做了全局混合，里面的方法全部的页面都可以使用
封装的方法主要分为几大类
1.提示信息Toast（）：wxShowToastNone wxShowToastSuccess
2.Loading: showLoadingMixin
3.跳转api: toNavigatePageMixin toRedirectToPageMixin toSwitchPageMixin
4.延时n秒后执行api： sleepMixin
```

